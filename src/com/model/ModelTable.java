package com.model;

import javax.swing.table.DefaultTableModel;
import java.util.Vector;

/**
 * @author Kilian Paquier
 */
public class ModelTable extends DefaultTableModel {
    /**
     * Cette fonction est une surcharge de getDataVector
     * @return Le vector de vector contenant les données du modèle de la table
     */
    public Vector<Vector> getDataVector() {
        Vector<Vector> dataVector = new Vector<>();
        Vector<String> ligne = new Vector<>();

        for (int i = 0; i < getRowCount(); i++) {
            for (int j = 0; j < getColumnCount(); j++) {
                if (getValueAt(i,j) == null)
                    ligne.addElement(null);
                else
                    ligne.addElement(getValueAt(i,j).toString());
            }
            dataVector.addElement(new Vector<>(ligne));
            ligne.clear();
        }
        return dataVector;
    }
}
