package com.model;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author Kilian Paquier
 * La classe de formatage, non instanciable elle contient des fonctions static
 */
public abstract class Formatage {

    /**
     * Cette fonction formate un fichier contenant une liste de dossiers non taxable en supprimant colonnes et lignes inutiles
     * @param file Le fichier à formater
     * @throws IOException Si le fichier n'existe pas ou qu'une autre erreur occure sur celui-ci
     */
    public static void formatageImportation(File file, boolean isNonTaxables) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1));
        Writer writer = new OutputStreamWriter(new FileOutputStream(file.getParent() + "\\Formaté " + file.getName(), false), StandardCharsets.ISO_8859_1);

        String ligneCourante;

        writer.write("Numéro du dossier,Nom du demandeur,Etat du dossier\n");

        while ((ligneCourante = reader.readLine()) != null) {
            ligneCourante = ligneCourante.replaceAll("\"", "");
            if (ligneCourante.split(",").length < 1 || ligneCourante.split(",")[0].length() < 14 || ligneCourante.split(",").length < 5)
                continue;
            if (!verifyPatternDossier(ligneCourante.split(",")[0]))
                continue;

            String dossier;
            String colonneDossier = ligneCourante.split(",")[0].replaceAll(" ", "").toUpperCase();
            String colonneDossierEspaces;
            if (colonneDossier.length() < 15)
                colonneDossierEspaces = colonneDossier.substring(0,2) + " " + colonneDossier.substring(2,5) + " " + colonneDossier.substring(5,8) + " "
                        + colonneDossier.substring(8,10) + " " + colonneDossier.substring(10,14);
            else
                colonneDossierEspaces = colonneDossier.substring(0,2) + " " + colonneDossier.substring(2,5) + " " + colonneDossier.substring(5,8) + " "
                        + colonneDossier.substring(8,10) + " " + colonneDossier.substring(10,15);

            if (ligneCourante.split(",")[1].equals("Inexistant") || ligneCourante.split(",")[1].equals("Innexistant")) {
                dossier = colonneDossierEspaces + "," + "Inexistant" + "," + "Erreur numérotation" + "\n";
            } else if (colonneDossier.substring(0, 2).equalsIgnoreCase("DP")) {
                if (ligneCourante.split(",")[2].length() == 0)
                    dossier = colonneDossierEspaces + "," + "Nom demandeur non précisé" + "," + "NON TAXABLE" + "\n";
                else
                    dossier = colonneDossierEspaces + "," + ligneCourante.split(",")[2] + "," + "NON TAXABLE" + "\n";
            }
            else {
                if (ligneCourante.split(",")[2].length() == 0)
                    dossier = colonneDossierEspaces + "," + "Nom demandeur non précisé" + "," + "Dossier refusé" + "\n";
                else
                    dossier = colonneDossierEspaces + "," + ligneCourante.split(",")[2] + "," + "Dossier refusé" + "\n";
            }

            dossier = dossier.replaceAll("\"", "");
            writer.write(dossier);
        }

        reader.close();
        writer.close();
    }

    private static boolean verifyPatternDossier(String s) {
        s = s.replaceAll("\"", "").replaceAll(" ", "");
        String type = s.substring(0, 2);
        if (!type.toUpperCase().equals("DP") && !type.toUpperCase().equals("PC") && !type.toUpperCase().equals("PA"))
            return false;
        String departement = s.substring(2, 5);
        String cummune = s.substring(5, 8);
        String annee = s.substring(9, 10);
        String numeroDossier = s.substring(11);
        try {
            Integer.parseInt(departement);
            Integer.valueOf(cummune);
            Integer.parseInt(annee);
            Integer.valueOf(numeroDossier);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Cette fonction formate un fichier CSV en un fichier XLS
     * @param file Le fichier à formater
     * @throws IOException Si le fichier n'existe pas ou qu'un problème occure sur celui-ci
     */
    public static void formatageXLS(File file) throws IOException {
        if (file.getName().contains(".csv")) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1));

            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("A");

            int index = 0;
            String ligne;
            while ((ligne = reader.readLine()) != null) {
                String[] cellules = ligne.split(",");
                HSSFRow row = sheet.createRow(index);

                int nbColonnes = 0;
                for (String cell : cellules) {
                    HSSFCell cellule = row.createCell(nbColonnes);
                    cellule.setCellValue(cell.replaceAll("\"", "").trim());
                    if (cell.length() == 0)
                        sheet.setColumnWidth(nbColonnes, (short) 15 * 512);
                    else
                        sheet.setColumnWidth(nbColonnes, (short) cell.length() * 512);
                    nbColonnes++;
                }
                index++;
            }

            FileOutputStream fileOutputStream = new FileOutputStream(file.getParentFile() + "\\" + file.getName().replace(".csv", "") + ".xls");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            reader.close();
        }
    }
}
