package com.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

//import java.io.Serializable;

/**
 * Classe DossierTaxe correspondant à un dossier taxés avec un ou plusieurs montants
 */
public class DossierTaxe {
    //private static final long serialVersionUID = -8554029811885485487L;

    private String numeroDossier;
    private String typeTA;
    private Integer montant;
    private String typeEIT;
    private LocalDate dateVerification;
    private LocalDate dateDecision;
    private LocalDate dateEcheance;
    private LocalDate dateEcheance2;
    private String type;
    private String annee;
    private Integer numDossier;
    private String typeTaxation;

    /**
     * Constructeur de confort prenant en paramètres un numéro de dossier, un type de TA, un montant et un type d'EIT
     * @param numeroDossier Le numéro du dossier
     * @param typeTA Le type de TA (TA Dep, TA Com, RAP2012)
     * @param montant Le montant de la TA pour le type en question
     * @param typeEIT Le type d'EIT (1 - 2 - Somme)
     * @param typeTaxation Le type de taxation du dossier (TI, TM, TR, AN)
     */
    public DossierTaxe(String numeroDossier, String typeTA, Integer montant, String typeEIT, String typeTaxation) {
        this.numeroDossier = numeroDossier;
        annee = numeroDossier.substring(11,13);
        type = numeroDossier.substring(0,2);
        this.typeTaxation = typeTaxation;
        this.typeEIT = typeEIT;
        this.typeTA = typeTA;
        this.montant = montant;
        this.numDossier = Integer.valueOf(numeroDossier.substring(15,19));
    }

    /**
     * Cette fonction permet de modifier le numéro du dossier
     * @param numeroDossier Le nouveau numéro de dossier
     */
    private void setNumeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
    }

    /**
     * Cette fonction permet de modifier le type de la TA
     * @param typeTA Le nouveau type de la TA
     */
    private void setTypeTA(String typeTA) {
        this.typeTA = typeTA;
    }

    /**
     * Cette fonction permet de modifier le montant de la TA
     * @param montant Le nouveau montant de la TA
     */
    private void setMontant(Integer montant) {
        this.montant = montant;
    }

    /**
     * Cette fonction permet de modifier le type de l'EIT
     * @param typeEIT Le nouveau type d'EIT
     */
    void setTypeEIT(String typeEIT) {
        this.typeEIT = typeEIT;
    }

    /**
     * Cette fonction permet de modifier la date de vérification de la TA pour le dossier en cours
     * @param dateVerification La nouvelle date de vérification
     */
    void setDateVerification(LocalDate dateVerification) {
        this.dateVerification = dateVerification;
    }

    /**
     * Cette fonction permet de modifier la date de décision de la TA pour le dossier en cours
     * @param dateDecision La nouvelle date de décision
     */
    void setDateDecision(LocalDate dateDecision) {
        this.dateDecision = dateDecision;
    }

    /**
     * Cette fonction retourne l'année du dossier (dépôt)
     * @return L'année du dossier
     */
    public String getAnnee() {
        return annee;
    }

    /**
     * Cette fonction retourne la type de TA
     * @return La type de TA
     */
    public String getTypeTA() {
        return typeTA;
    }

    /**
     * Cette fonction retourne le numéro du dossier
     * @return La numéro du dossier
     */
    public String getNumeroDossier() {
        return numeroDossier;
    }

    /**
     * Cette fonction retourne la date de vérification
     * @return La date de vérification
     */
    public LocalDate getDateVerification() {
        return dateVerification;
    }

    /**
     * Cette fonction retourne la date de décision
     * @return La date de décision
     */
    public LocalDate getDateDecision() {
        return dateDecision;
    }

    /**
     * Cette fonction retourne le type du dossier (DP, PC, PA)
     * @return Le type du dossier
     */
    public String getType() {
        return type;
    }

    /**
     * Cette fonction compare deux informations de dossiers taxés sur la base du numéro de dossier, du type de TA et du type d'EIT
     * @param obj Le dossier taxé comparé avec l'objet courant
     * @return Vrai si les dossiers sont égaux, faux sinon
     */
    boolean equalsWithEIT(DossierTaxe obj) {
        return obj.numeroDossier.equals(numeroDossier) && obj.typeTA.equals(typeTA) && obj.typeEIT.equals(typeEIT) && obj.typeTaxation.equals(typeTaxation);
    }

    /**
     * Cette fonction compare deux dossiers taxés sur la base du numéro de dossier et du type de TA
     * @param obj Le dossier taxé comparé avec l'objet courant
     * @return Vrai si les dossiers sont égaux, faux sinon
     */
    boolean equalsWithoutEIT(DossierTaxe obj) {
        return obj.numeroDossier.equals(numeroDossier) && obj.typeTA.equals(typeTA) && obj.typeTaxation.equals(typeTaxation);
    }

    /**
     * Cette fonction retourne le type d'EIT
     * @return Le type d'EIT
     */
    public String getTypeEIT() {
        return typeEIT;
    }

    /**
     * Cette fonction retourne le montant
     * @return Le montant
     */
    public Integer getMontant() {
        return montant;
    }

    @Override
    public String toString() {
        return numeroDossier + " " + dateVerification + " "+ typeTA + " " + typeEIT + " " + montant;
    }

    /**
     * Cette fonction permet de définir le choix des attributs à enregistrer lors de la sérialisation
     * @param objectOutputStream L'objectOutputStream qui écrit les données dans le fichier de sérialisation
     * @throws IOException Si un problème a lieu lors de l'écrire ou que l'objectOutputStream est null
     */
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(numeroDossier);
        objectOutputStream.flush();
        objectOutputStream.writeObject(typeTA);
        objectOutputStream.flush();
        objectOutputStream.writeObject(montant);
        objectOutputStream.flush();
        objectOutputStream.writeObject(typeEIT);
        objectOutputStream.flush();
        objectOutputStream.writeObject(dateVerification);
        objectOutputStream.flush();
        objectOutputStream.writeObject(dateDecision);
        objectOutputStream.flush();
        objectOutputStream.writeObject(typeTaxation);
        objectOutputStream.flush();
        objectOutputStream.writeObject(dateEcheance);
        objectOutputStream.flush();
        objectOutputStream.writeObject(dateEcheance2);
        objectOutputStream.flush();
    }

    /**
     * Cette fonction permet de lire l'objet précédemment enregistré correctement
     * @param objectInputStream L'objectInputStream qui lit les données dans le fichier de sérialisation
     * @throws IOException Si un problème a lieu lors de la lecture ou que l'objectInputStream est null
     * @throws ClassNotFoundException Si la classe de cast n'existe pas
     */
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        setNumeroDossier((String) objectInputStream.readObject());
        setTypeTA((String) objectInputStream.readObject());
        setMontant((Integer) objectInputStream.readObject());
        setTypeEIT((String) objectInputStream.readObject());
        setDateVerification((LocalDate) objectInputStream.readObject());
        setDateDecision((LocalDate) objectInputStream.readObject());
        setTypeTaxation((String) objectInputStream.readObject());
        setDateEcheance((LocalDate) objectInputStream.readObject());
        setDateEcheance2((LocalDate) objectInputStream.readObject());

        this.type = this.numeroDossier.substring(0,2);
        this.annee = this.numeroDossier.substring(11,13);
        this.numDossier = Integer.valueOf(this.numeroDossier.substring(15,19));
    }

    /**
     * Cette fonction retourne le numéro de dossier
     * @return Le numéro de dossier
     */
    Integer getNumDossier() {
        return numDossier;
    }

    /**
     * Cette fonction ajoute un montant au montant actuel
     * @param montant Le montant à ajouter au montant actuel
     */
    void ajouterMontant(Integer montant) {
        this.montant += montant;
    }

    /**
     * Cette fonction retourne le type de taxation du dossier
     * @return Le type de taxation du dossier
     */
    public String getTypeTaxation() {
        return typeTaxation;
    }

    /**
     * Cette fonction change le type de taxation du dossier
     * @param typeTaxation Le nouveau type de taxation (TI, TM, TR)
     */
    private void setTypeTaxation(String typeTaxation) {
        this.typeTaxation = typeTaxation;
    }

    /**
     * Cette fonction modifie la date d'échéance avec une nouvelle date
     * @param dateEcheance La nouvelle date d'échéance
     */
    void setDateEcheance(LocalDate dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    /**
     * Cette fonction retourne la date échéance à laquelle le montant devra être payé
     * @return La date d'échéance
     */
    public LocalDate getDateEcheance() {
        return dateEcheance;
    }

    public LocalDate getDateEcheance2() {
        return dateEcheance2;
    }

    void setDateEcheance2(LocalDate dateEcheance2) {
        this.dateEcheance2 = dateEcheance2;
    }
}
