package com.view.java;

import com.controler.DocControler;
import com.model.Menu;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Documentation extends JFrame {
    private JPanel globalPanel;
    private JTree tree;
    private JTextPane textPane;
    private boolean open;

    public Documentation(String title) {
        setTitle(title);
        setContentPane(globalPanel);
        initTree();
        setIconImage(new ImageIcon(".\\resources\\icon.png").getImage());
        DocControler.setTexteVuePrincipale(textPane);
    }

    private void initTree() {
        DefaultMutableTreeNode menu = new DefaultMutableTreeNode("Documentation");

        DefaultMutableTreeNode communes = new DefaultMutableTreeNode("Gestion des communes");
        DefaultMutableTreeNode dossiers = new DefaultMutableTreeNode("Gestion des dossiers");
        DefaultMutableTreeNode montants = new DefaultMutableTreeNode("Information sur les montants");
        DefaultMutableTreeNode format = new DefaultMutableTreeNode("Formatage");

        format.add(new DefaultMutableTreeNode("Conversion de CSV vers XLS"));
        format.add(new DefaultMutableTreeNode("Formatage de fichiers pour importation"));

        montants.add(new DefaultMutableTreeNode("Importation de montants"));
        montants.add(new DefaultMutableTreeNode("Visualisation de montants"));
        montants.add(new DefaultMutableTreeNode("Edition de tableaux croisés"));
        montants.add(new DefaultMutableTreeNode("Exportation de tableaux croisés des montants"));

        dossiers.add(new DefaultMutableTreeNode("Importation de dossiers"));
        dossiers.add(new DefaultMutableTreeNode("Visualisation de dossiers"));
        dossiers.add(new DefaultMutableTreeNode("Edition de tableaux croisés"));
        dossiers.add(new DefaultMutableTreeNode("Modification d'un dossier"));

        communes.add(new DefaultMutableTreeNode("Importation de communes"));
        communes.add(new DefaultMutableTreeNode("Ajout d'une commune"));
        communes.add(new DefaultMutableTreeNode("Ajout d'exceptions"));
        communes.add(new DefaultMutableTreeNode("Importation d'une liste d'exceptions"));

        menu.add(communes);
        menu.add(dossiers);
        menu.add(montants);
        menu.add(format);
        menu.add(new DefaultMutableTreeNode("Vue principale"));
        menu.add(new DefaultMutableTreeNode("Suivi des mises à jours"));

        DefaultTreeModel model = new DefaultTreeModel(menu);
        tree.setModel(model);
        tree.setCellRenderer(new Menu.MyRenderer());
        for (int row = 0; row < tree.getRowCount(); row++)
            tree.expandRow(row);

        tree.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                JTree jTree = (JTree) e.getSource();
                TreePath path = jTree.getPathForLocation(e.getX(), e.getY());
                //int row = jTree.getRowForLocation(e.getX(), e.getY());
                if (path != null) {
                    jTree.setCursor(new Cursor(Cursor.HAND_CURSOR));
                }
                else {
                    jTree.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }
        });

        tree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                JTree jTree = (JTree) e.getSource();
                TreePath path = jTree.getPathForLocation(e.getX(), e.getY());
                if (path != null) {
                    DefaultMutableTreeNode item = (DefaultMutableTreeNode) path.getLastPathComponent();
                    //DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                    if (item != null) {
                        if (item.toString().equals("Importation de communes"))
                            DocControler.setTexteImporterCommunes(textPane);
                        if (item.toString().equals("Ajout d'une commune"))
                            DocControler.setTexteAjoutCommune(textPane);
                        if (item.toString().equals("Ajout d'exceptions"))
                            DocControler.setTexteAjoutException(textPane);
                        if (item.toString().equals("Vue principale"))
                            DocControler.setTexteVuePrincipale(textPane);
                        if (item.toString().equals("Importation de dossiers"))
                            DocControler.setTexteImporterDossiers(textPane);
                        if (item.toString().equals("Visualisation de dossiers"))
                            DocControler.setTexteEditionDossiers(textPane);
                        if (item.toString().equals("Edition de tableaux croisés") && item.getParent().toString().equals("Gestion des dossiers"))
                            DocControler.setTexteEditionTableaux(textPane);
                        if (item.toString().equals("Modification d'un dossier"))
                            DocControler.setTexteModifierDossier(textPane);
                        if (item.toString().equals("Importation de montants"))
                            DocControler.setTexteImporterTaxes(textPane);
                        if (item.toString().equals("Visualisation de montants"))
                            DocControler.setTexteEditionTA(textPane);
                        if (item.toString().equals("Edition de tableaux croisés") && item.getParent().toString().equals("Information sur les montants"))
                            DocControler.setTexteTableauxTA(textPane);
                        if (item.toString().equals("Conversion de CSV vers XLS"))
                            DocControler.setTexteFormatageXLS(textPane);
                        if (item.toString().equals("Formatage de fichiers pour importation"))
                            DocControler.setTexteFormatageNT(textPane);
                        if (item.toString().equals("Suivi des mises à jours"))
                            DocControler.setTexteMAJ(textPane);
                        if (item.toString().equals("Exportation de tableaux croisés des montants"))
                            DocControler.setTexteExportTA(textPane);
                        if (item.toString().equals("Importation d'une liste d'exceptions"))
                            DocControler.setTexteImportExceptions(textPane);
                    }
                }
            }
        });
    }

    public void open() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                open = false;
            }
        });
        pack();
        setSize(1300, 650);
        setLocationRelativeTo(null);
        setVisible(true);
        open = true;
    }

    public boolean isOpen() {
        return open;
    }
}
