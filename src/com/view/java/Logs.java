package com.view.java;

import javax.swing.*;

public class Logs extends JDialog {

    private JPanel globalPanel;
    private JTextPane textPane;

    public Logs(JFrame view, StringBuilder logs) {
        setTitle("Logs");
        setContentPane(globalPanel);
        textPane.setText(logs.toString());
        openDialog(view);
    }

    private void openDialog(JFrame view) {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(900, 450);
        setLocationRelativeTo(view);
        setVisible(true);
    }
}
