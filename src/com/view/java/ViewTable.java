package com.view.java;

import javax.swing.*;

public class ViewTable extends JDialog {
    private JPanel globalPanel;
    private JTable table;
    private JButton exporterButton;
    private JTextField nbDossiers;
    private JLabel nbDossiersTxt;
    private JButton exporterToutesLesCommunesButton;

    public ViewTable(JDialog view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
    }

    public void ouvrir() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(1100, 650);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }

    public JTable getTable() {
        return table;
    }

    public JButton getExporterButton() {
        return exporterButton;
    }

    public JTextField getNbDossiers() {
        return nbDossiers;
    }

    public JLabel getNbDossiersTxt() {
        return nbDossiersTxt;
    }

    public JButton getExporterToutesLesCommunesButton() {
        return exporterToutesLesCommunesButton;
    }

}
