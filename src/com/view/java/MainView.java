package com.view.java;

import javax.swing.*;

public class MainView extends JFrame {
    private JPanel globalPanel;
    private JTree menu;
    private JTable tableCommunes;
    private JTextField nbCommunesEPCI;
    private JButton toutDecocherButton;
    private JCheckBox dataBox;
    private JButton documentationButton;

    public MainView(String title) {
        setContentPane(globalPanel);
        setTitle(title);
        setIconImage(new ImageIcon(".\\resources\\ico\\icon.png").getImage());
    }

    public JPanel getGlobalPanel() {
        return globalPanel;
    }

    public JTree getMenu() {
        return menu;
    }

    public JTable getTableCommunes() {
        return tableCommunes;
    }

    public JTextField getNbCommunesEPCI() {
        return nbCommunesEPCI;
    }

    public JButton getToutDecocherButton() {
        return toutDecocherButton;
    }

    public JCheckBox getDataBox() {
        return dataBox;
    }

    public JButton getDocumentationButton() {
        return documentationButton;
    }
}
