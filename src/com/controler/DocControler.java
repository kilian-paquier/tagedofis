package com.controler;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author Kilian Paquier
 * Classe controler de la documentation
 */
abstract public class DocControler {

    public static void setTexteImporterDossiers(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\importDossier.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                if (!ligne.contains(".\\\\resources\\\\png\\\\")) {
                    document.insertString(document.getLength(), ligne + "\n", null);
                } else {
                    Style image = document.addStyle("Image", null);
                    ImageIcon icon = new ImageIcon(ligne);
                    StyleConstants.setIcon(image, icon);
                    document.insertString(document.getLength(), "\uFFFC", image);
                }
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteImporterCommunes(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\importCommunes.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                if (!ligne.contains(".\\\\resources\\\\png\\\\")) {
                    document.insertString(document.getLength(), ligne + "\n", null);
                } else {
                    Style image = document.addStyle("Image", null);
                    ImageIcon icon = new ImageIcon(ligne);
                    StyleConstants.setIcon(image, icon);
                    document.insertString(document.getLength(), "\uFFFC", image);
                }
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteEditionDossiers(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\visualisationDossiers.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }

    }

    public static void setTexteModifierDossier(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\modifDossier.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteImporterTaxes(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\importMontants.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                if (!ligne.contains(".\\\\resources\\\\png\\\\")) {
                    document.insertString(document.getLength(), ligne + "\n", null);
                } else {
                    Style image = document.addStyle("Image", null);
                    ImageIcon icon = new ImageIcon(ligne);
                    StyleConstants.setIcon(image, icon);
                    document.insertString(document.getLength(), "\uFFFC", image);
                }
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteEditionTableaux(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\tableauxCroisés.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteEditionTA(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\mainView.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteAjoutCommune(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\ajoutCommunes.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteFormatageNT(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\format.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                if (!ligne.contains(".\\\\resources\\\\png\\\\")) {
                    document.insertString(document.getLength(), ligne + "\n", null);
                } else {
                    Style image = document.addStyle("Image", null);
                    ImageIcon icon = new ImageIcon(ligne);
                    StyleConstants.setIcon(image, icon);
                    document.insertString(document.getLength(), "\uFFFC", image);
                }
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteFormatageXLS(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\formatageXLS.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteVuePrincipale(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\mainView.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteTableauxTA(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\editionTA.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteExportTA(JTextPane textArea) {
        try {
            textArea.setText("");
            StyledDocument document = textArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\exportTA.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textArea.setDocument(document);
            textArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteAjoutException(JTextPane texteArea) {
        try {
            texteArea.setText("");
            StyledDocument document = texteArea.getStyledDocument();
            File file = new File(".\\resources\\txt\\ajoutException.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            texteArea.setDocument(document);
            texteArea.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteMAJ(JTextPane textPane) {
        try {
            textPane.setText("");
            StyledDocument document = textPane.getStyledDocument();
            File file = new File(".\\resources\\txt\\maj.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                document.insertString(document.getLength(), ligne + "\n", null);
            }
            textPane.setDocument(document);
            textPane.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void setTexteImportExceptions(JTextPane textPane) {
        try {
            textPane.setText("");
            StyledDocument document = textPane.getStyledDocument();
            File file = new File(".\\resources\\txt\\importExceptions.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
            String ligne = null;
            while ((ligne = reader.readLine()) != null) {
                if (!ligne.contains(".\\\\resources\\\\png\\\\")) {
                    document.insertString(document.getLength(), ligne + "\n", null);
                } else {
                    Style image = document.addStyle("Image", null);
                    ImageIcon icon = new ImageIcon(ligne);
                    StyleConstants.setIcon(image, icon);
                    document.insertString(document.getLength(), "\uFFFC", image);
                }
            }
            textPane.setDocument(document);
            textPane.setCaretPosition(0);
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }
}
